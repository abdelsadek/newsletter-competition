

##To install project 
- Run Composer install
- Add database config to .env file
- run "php artisan migrate --seed"
- run "php artisan serve"
- api url "http://localhost:800/api"
- import the postman collection to test "https://www.getpostman.com/collections/daa0f3120e24c6eb9f67"
