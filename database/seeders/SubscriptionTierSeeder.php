<?php

namespace Database\Seeders;

use App\Models\SubscriptionTier;
use Illuminate\Database\Seeder;

class SubscriptionTierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubscriptionTier::insert([
            [
                'name' => 'Daily',
                'price' => 30
            ],
            [
                'name' => 'Weekly',
                'price' => 20
            ],
            [
                'name' => 'Monthly',
                'price' => 10
            ],
        ]);
    }
}
