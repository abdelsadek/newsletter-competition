<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscribers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->unsignedInteger('subscription_tier_id');
            $table->enum('payment_method',['cash','credit_card'])->default('cash');
            $table->decimal('balance')->default(0);
            $table->string('card_number')->nullable();
            $table->date('expires_in')->nullable();
            $table->char('cvv',3)->nullable();
            $table->enum('status',['subscribed','unsubscribed'])->default('subscribed');
            $table->timestamps();

            $table->foreign('subscription_tier_id')->references('id')->on('subscription_tiers')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscripers');
    }
}
