<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('subscribe','SubscriberAPIController@subscribe');
Route::post('unsubscribe','SubscriberAPIController@unsubscribe');
Route::post('addNewsletter','NewsletterAPIController@store');
Route::get('myNewsletters','NewsletterAPIController@myNewsletters');
Route::get('paymentHistory','SubscriberAPIController@paymentHistory');
Route::post('changePrice','SubscriptionTierAPIController@changePrice');
