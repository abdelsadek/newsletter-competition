<?php

namespace App\Listeners;

use App\Events\NewsletterCreated;
use App\Models\Payment;
use App\Models\Subscriber;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class MakePaymentTransaction
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewsletterCreated  $event
     * @return void
     */
    public function handle(NewsletterCreated $event)
    {
         $subscribers = Subscriber::all();
         foreach ($subscribers as $subscriber){
            Payment::pay($subscriber, $event->newsletter);
         }
    }
}
