<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'amount',
        'payment_method',
        'subscriber_id',
        'newsletter_id'
    ];

    public static function pay($subscriber, $newsletter){
        if($subscriber->payment_method == 'cash'){
            static::payCash($subscriber, $newsletter);
        }
        if($subscriber->payment_method == 'credit_card'){
            static::payCreditCard($subscriber, $newsletter);
        }
    }

    public static function payCash($subscriber, $newsletter){
        if($subscriber->balance >= $subscriber->subscriptionTier->price){
            static::create([
                'amount' => $subscriber->subscriptionTier->price,
                'subscriber_id' => $subscriber->id,
                'payment_method' => 'cash',
                'newsletter_id' => $newsletter->id
            ]);
            $subscriber->balance = $subscriber->balance - $subscriber->subscriptionTier->price;
            $subscriber->save();
        }
    }
    public static function payCreditCard($subscriber, $newsletter){
        $paySuccess = static::externalPayment($subscriber);
        if($paySuccess) {
             static::create([
                'amount' => $subscriber->subscriptionTier->price,
                'subscriber_id' => $subscriber->id,
                'payment_method' => 'credit_card',
            'newsletter_id' => $newsletter->id
            ]);
        }

    }

    public static function externalPayment($subscriber){
        return rand(0,1) == 1;
    }
}
