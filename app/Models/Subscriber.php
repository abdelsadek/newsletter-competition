<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'subscription_tier_id',
        'payment_method',
        'balance',
        'card_number',
        'expires_in',
        'cvv',
        'status'
    ];

    public function subscriptionTier()
    {
        return $this->belongsTo(SubscriptionTier::class);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // auto-sets values on creation
        static::creating(function ($query) {
            $query->balance =mt_rand(10,1000);;
        });
    }

    public function newsletters()
    {
        return $this->belongsToMany(Newsletter::class);
    }
    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}
