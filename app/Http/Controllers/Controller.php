<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Response;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendResponse($data, $message)
    {
        return Response::json(
            [
                'success' => true,
                'data'    => $data,
                'message' => $message,
            ]
        );
    }
    public function sendError($data, $message)
    {
        return Response::json(
            [
                'success' => false,
                'message' => $message,
            ]
        );
    }
}
