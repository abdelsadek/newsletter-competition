<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubscribeRequest;
use App\Http\Requests\UnsubscribeRequest;
use App\Models\Subscriber;
use Illuminate\Http\Request;

class SubscriberAPIController extends Controller
{
    public function subscribe(SubscribeRequest $request)
    {
        $input = $request->all();
        $input['status'] = 'subscribed';
        $subscriber = Subscriber::updateOrCreate(['email' => $input['email']],$input);

        return $this->sendResponse(
            $subscriber,
            'You have subscribed successfully'
        );

    }
    public function unsubscribe(UnsubscribeRequest $request)
    {
        $input = $request->all();
        $subscriber = Subscriber::where('email', $input['email'])->first();
        $subscriber->status = 'unsubscribed';
        $subscriber->save();
        return $this->sendResponse(
            $subscriber,
            'You have unsubscribed successfully'
        );

    }

    public function paymentHistory(Request $request)
    {
        $email = $request->get('email');
        $subscriber = Subscriber::where('email',$email)->first();
        $newsletters = $subscriber->payments;
         return $this->sendResponse(
            $newsletters,
            'Your payment history retrieved'
        );
    }
}
