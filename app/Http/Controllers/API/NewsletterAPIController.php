<?php

namespace App\Http\Controllers\API;

use App\Events\NewsletterCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateNewsletterRequest;
use App\Models\Newsletter;
use App\Models\Subscriber;
use Illuminate\Http\Request;

class NewsletterAPIController extends Controller
{
    public function store(CreateNewsletterRequest $request)
    {
        $input = $request->all();
        $newsletter = Newsletter::create($input);
        $newsletter->subscribers()->attach(Subscriber::pluck('id'));
        event(new NewsletterCreated($newsletter));
        return $this->sendResponse(
            $newsletter,
            'Newsletter created successfully'
        );
    }

    public function myNewsletters(Request $request)
    {
        $email = $request->get('email');
        $subscriber = Subscriber::where('email',$email)->first();
        $newsletters = $subscriber->newsletters;
         return $this->sendResponse(
            $newsletters,
            'Your Newsletters retrieved'
        );
    }
}
