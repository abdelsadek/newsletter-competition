<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangeTierPriceRequest;
use App\Models\SubscriptionTier;
use Illuminate\Http\Request;

class SubscriptionTierAPIController extends Controller
{
    public function changePrice(ChangeTierPriceRequest $request)
    {
        $subscriptionTier = SubscriptionTier::find($request->get('id'));
        $subscriptionTier->price = $request->get('price');
        $subscriptionTier->save();
        return $this->sendResponse(
            $subscriptionTier,
            'SubscriptionTier updated successfully'
        );
    }
}
